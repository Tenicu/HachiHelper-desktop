This is a simple linux Java application that grabs reports from https://8chan.net/mod.php?/reports and posts them back as desktop notifications.

Presently, it is necessary to paste your access cookie into the settings file. Whether the other strings (user-agent and so on) matter is as yet unknown, if it doesn't work try replacing these strings with your own (cloudflare may be matching these with your __cfduid or something). 

Top to bottom in the settings file (one per line):
>Cookie

>User-Agent

>Accept

>Accept-Encoding

>Connection

>Time between refreshes in milliseconds (you can put any number you want here)
    
The reason this is linux-only is down to the notifier itself being notify-send, it shouldn't be too hard to swap in Windows/Mac support if you feel like it.

On that note, make sure you have notify-send installed or it won't work, it'll probably already be installed but if not you should be able to grab it with
>sudo apt-get install notify-osd

The reasoning behind doing this in >java here is that this code is going to end up in an android application when I get around to it, but I thought it might be good to split it off into its own thing as well.

Compilation can be done with:
>cd src

>javac -g HachiHelper.java

>jar cvfm HachiHelper.jar manifest.mf HachiHelper.class org

The application can then be executed with:
>java -jar HachiHelper.jar

A pre-compiled .jar file is also included if you'd rather not bother compiling yourself. Note: Make sure you remember to put the settings.txt file in the same directory you're trying to run the .jar from.

Todo: 

-Strip HTML tags from reports

-Keep track of reports & include option to only dispatch each report once
