import java.net.*;
import java.io.*;
import java.util.*;
import javax.net.ssl.*;
import java.util.zip.*;
import org.jsoup.*;
import org.jsoup.nodes.*;
import org.jsoup.select.*;

/**
 * Created by Tenicu on 13/03/15.
 */

public class HachiHelper {

    static final String COOKIES_HEADER = "Set-Cookie";
    static final int maxNotificationLength = 35;//dont want too much text in the notifications

    public static String[] getSettings(String filename) throws Exception {
        int i = 0;
        String[] settings = new String[6];
        
        Scanner scan = new Scanner(new File(filename));
        while (i < 6 && scan.hasNextLine())
            settings[i++] = scan.nextLine();
        scan.close();
        
        return settings;
    }

    public static String getReports(String propCookie, String propUserAgent, String propAccept, String propEncoding, String propConnection) throws Exception {
        /*Scanner scan = new Scanner(new File("settings.txt"));
        String propCookie = scan.nextLine();
        String propUserAgent = scan.nextLine();
        String propAccept = scan.nextLine();
        String propEncoding = scan.nextLine();
        String propConnection = scan.nextLine();
        scan.close();*/

        URL modURL = new URL("https://8ch.net/mod.php?/reports");
        HttpsURLConnection conn = (HttpsURLConnection)modURL.openConnection();
        
        conn.setRequestProperty("User-Agent", propUserAgent);
        conn.setRequestProperty("Accept", propAccept);
        //conn.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
        conn.setRequestProperty("Accept-Encoding", propEncoding);
        //conn.setRequestProperty("DNT" , "1");
        //conn.setRequestProperty("Referer", "https://8ch.net/mod.php?/reports");
        conn.setRequestProperty("Cookie", propCookie);
        conn.setRequestProperty("Connection", propConnection);
	    
	    conn.connect();

	    Map<String, List<String>> headerFields = conn.getHeaderFields();
	    List<String> cookiesHeader = headerFields.get(COOKIES_HEADER);

        System.out.println (headerFields);
        	
	    String headerName=null;
	    for (int i=1; (headerName = conn.getHeaderFieldKey(i))!=null; i++) {
	     	if (headerName.equals(COOKIES_HEADER)) {                  
			    String cookie = conn.getHeaderField(i);       
			    cookie = cookie.substring(0, cookie.indexOf(";"));
			    String cookieName = cookie.substring(0, cookie.indexOf("="));
			    String cookieValue = cookie.substring(cookie.indexOf("=") + 1, cookie.length());

			    //if (cookieName.equals("__cfduid"))
			      //  cfduid = cookieValue;
		    }
	    }

        InputStream response = conn.getInputStream();
/*        int i;
        while ((i = response.read()) != -1){
            char ch = (char)i;
            System.out.println( i);
        }
*/
        //BufferedReader in = new BufferedReader(new InputStreamReader(modURL.openStream()));
        BufferedReader in = new BufferedReader(new InputStreamReader(new GZIPInputStream(response)));

        String inputLine;
        if ((inputLine = in.readLine()) != null){
            //System.out.println(inputLine);
        }else{
            System.out.println("No response from server.");
        }
        in.close();
        
        return inputLine;
    }

    public static List<List<String>> parseReports(String input) {
        Document document = Jsoup.parse(input);
        Elements reports = document.getElementsByClass("body");
        Elements reportReasons = document.getElementsByClass("report-list");

        List<List<String>> parsedReports = new ArrayList<List<String>>();

        for (int i = 0; i < reports.size(); i++){
            List<String> singleReport = new ArrayList<String>();
            singleReport.add(reports.get(i).html());
            
            for (int j = 0; j < reportReasons.get(i).getElementsByClass("report-reason").size(); j++){
                singleReport.add(reportReasons.get(i).getElementsByClass("report-reason").get(j).html());
            }
            
            parsedReports.add(singleReport);
        }
        
        return parsedReports;
    }
    
    public static void postNotifications(List<List<String>> input) throws Exception {
        //Post notifications to the desktop
        Runtime myRuntime = Runtime.getRuntime();
        
        for (List<String> s : input){    
            
            String[] output = new String[5];
            output[0] = "notify-send";
            output[1] = s.get(0);
            output[2] = s.get(1);
            output[3] = "-i";
            output[4] = "user-trash";
            
            if (output[1].length() >= maxNotificationLength)
                output[1] = output[1].substring(0, maxNotificationLength);
                
            if (output[2].length() >= maxNotificationLength)
                output[2] = output[2].substring(0, maxNotificationLength);
                
            //for now only use the first report
            System.out.println("Posting report: " + output[1] + "\nReason: " + output[2]);
            myRuntime.exec(output);
        }
    }

    public static void main(String[] args) throws Exception {
        String[] settings = getSettings("settings.txt");
        int sleepTime = 0;
        
        try {
            sleepTime = Integer.parseInt(settings[5]);
        } catch (Exception e){
            sleepTime = 900000;
        }
        
        System.out.println("Checking for new reports every " + (sleepTime / 60 / 1000) + " minutes");
    
        while (true){
            String rawPage = getReports(settings[0], settings[1], settings[2], settings[3], settings[4]);
        
            List<List<String>> parsedInput = parseReports(rawPage);
            
            postNotifications(parsedInput);
        
            try {
                Thread.sleep(sleepTime);
            } catch (Exception e){
                //dont care
            }
        }
    }
}

